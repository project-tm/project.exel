<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

global $USER;
if (!$USER->IsAuthorized())
    exit;

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'ID')
        ->setCellValue('B1', 'Номер карты')
        ->setCellValue('C1', 'E-mail')
        ->setCellValue('D1', 'Отправлено');
$excel->getActiveSheet()->setTitle('Почта с mail.ru');

Bitrix\Main\Loader::includeModule('iblock');
$applications = array();
$appsFilter = array(
    '=IBLOCK_ID' => \Tools\Constants::$IBLOCK_APPLICATIONS,
    '<=DATE_CREATE' => '30.09.2016 16:00:00'
);
$dbApps = CIBlockElement::GetList(array('ID'=>'DESC'), $appsFilter, false, false, array(
            'ID',
            'PROPERTY_EMAIL',
            'PROPERTY_CARD',
            'PROPERTY_IS_SEND',
            'PROPERTY_CARD.NAME',
        ));

while ($app = $dbApps->Fetch()) {
    if (\Local\Helpers\Applications::isMail($app['PROPERTY_EMAIL_VALUE'])) {
        $applications[] = array(
            'id' => $app['ID'],
            'card' => $app['PROPERTY_CARD_NAME'],
            'email' => $app['PROPERTY_EMAIL_VALUE'],
            'is_send' => $app['PROPERTY_IS_SEND_VALUE'],
        );
    }
}

foreach ($applications as $i => $application) {
    $row = $i + 2;
    $excel->getActiveSheet()
            ->setCellValue('A' . $row, $application['id'])
            ->setCellValue('D' . $row, $application['card'])
            ->setCellValue('C' . $row, $application['email'])
            ->setCellValue('D' . $row, $application['is_send']);
}

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

$filename = $_SERVER['DOCUMENT_ROOT'] . '/local/tools/report.mail.xlsx';
$objWriter->save($filename);

$filename = __DIR__ . '/report.mail.xlsx';

if (file_exists($filename)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    exit;
}